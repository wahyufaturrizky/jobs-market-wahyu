import { AuthDTO } from '@app/interface/auth';
import { UserInterface } from '@app/interface/users';
import { Action } from '@ngrx/store';

export enum AuthActionTypes {
  LOGIN_USER = '[AUTH] Login User',
  REGISTER_USER = '[AUTH] Register User',
  SET_CURRENT_USER = '[AUTH] Set current user',
  SET_INITIAL_USER = '[AUTH] Set initial user',
}

export class LoginUser implements Action {
  readonly type = AuthActionTypes.LOGIN_USER;
  constructor(public payload: AuthDTO) {}
}

export class RegisterUser implements Action {
  readonly type = AuthActionTypes.REGISTER_USER;
  constructor(public payload: AuthDTO) {}
}

export class SetInitialUser implements Action {
  readonly type = AuthActionTypes.SET_INITIAL_USER;
  constructor() {}
}

export class SetCurrentUser implements Action {
  readonly type = AuthActionTypes.SET_CURRENT_USER;
  constructor(public payload: UserInterface | null) {}
}

export type ActionAuth =
  | LoginUser
  | RegisterUser
  | SetInitialUser
  | SetCurrentUser;
