import { Injectable } from '@angular/core';
import {
  AuthActionTypes,
  LoginUser,
  RegisterUser,
  SetCurrentUser,
  SetInitialUser,
} from '@app/app-store/actions/auth.action';
import { AddError, RemoveError } from '@app/app-store/actions/errors.action';
import { AppStoreModule } from '@app/app-store/app-store.module';
import { UserInterface } from '@app/interface/users';
import { AuthService } from '@app/services/auth.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';

@Injectable()
export class AuthEffects {
  constructor(
    private action$: Actions,
    private authService: AuthService,
    private store: Store<AppStoreModule>
  ) {}

  @Effect()
  setInitialUser$: Observable<Action> = this.action$.pipe(
    ofType<SetInitialUser>(AuthActionTypes.SET_INITIAL_USER),
    tap(() => this.store.dispatch(new RemoveError())),
    mergeMap((action: SetInitialUser) =>
      this.authService.whoami().pipe(
        map((user: UserInterface) => new SetCurrentUser(user)),
        catchError((err) => of(new AddError(err)))
      )
    )
  );

  @Effect()
  loginUser$: Observable<Action> = this.action$.pipe(
    ofType<LoginUser>(AuthActionTypes.LOGIN_USER),
    tap(() => this.store.dispatch(new RemoveError())),
    mergeMap((action: LoginUser) =>
      this.authService.login(action.payload).pipe(
        map((user: UserInterface) => new SetCurrentUser(user)),
        catchError((err) => of(new AddError(err)))
      )
    )
  );

  @Effect()
  registerUser$: Observable<Action> = this.action$.pipe(
    ofType<RegisterUser>(AuthActionTypes.REGISTER_USER),
    tap(() => this.store.dispatch(new RemoveError())),
    mergeMap((action: RegisterUser) =>
      this.authService.register(action.payload).pipe(
        map((user: UserInterface) => new SetCurrentUser(user)),
        catchError((err) => of(new AddError(err)))
      )
    )
  );
}
