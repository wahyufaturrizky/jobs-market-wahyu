import { Component, OnInit } from '@angular/core';
import { SetInitialUser } from '@app/app-store/actions/auth.action';
import { AppState } from '@app/app-store/app-store.module';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'jobs-market-wahyu';

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.store.dispatch(new SetInitialUser());
  }
}
