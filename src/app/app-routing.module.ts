import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from '@app/components/auth/auth.component';
import { PageNotFoundComponent } from '@app/screens/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
  },
  {
    path: 'users',
    loadChildren: () =>
      import('@app/features/user/user.module').then((mod) => mod.UserModule),
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
