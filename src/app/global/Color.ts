export enum ColorBaseEnum {
  black = '#000',
  white = '#FFF',
}

export enum ColorPrimaryEnum {
  cream = '#FCF0DE',
  blue = '#205C8C',
  orange = '#FF6C4A',
  red = '#E22153',
  green = 'rgb(11,104,80)',
  yellow = '#FCAD10',
  grey = '#E5E5E5',
}

export enum ColorBaseGrayEnum {
  gray100 = '#FAFAFA',
  gray200 = '#F5F5F5',
  gray300 = '#EBEBEB',
  gray400 = '#BDBDBD',
  gray500 = '#9E9E9E',
  gray600 = '#555555',
  gray700 = '#1A1A1A',
}

export enum ColorSemanticInfoEnum {
  darker = '#09509D',
  dark = '#2371C6',
  default = '#4393EA',
  light = '#D3E5FA',
  lighter = '#F0F7FF',
}
export enum ColorSemanticPositiveEnum {
  darker = '#0B6732',
  dark = '#19954D',
  default = '#27AE60',
  light = '#BEF0D3',
  lighter = '#E3F6EB',
}

export enum ColorSemanticWarningEnum {
  darker = '#904E01',
  dark = '#D97A0B',
  default = '#FF8A00',
  light = '#FFE7CA',
  lighter = '#FFF8EF',
}

export enum ColorSemanticDangerEnum {
  darker = '#76102B',
  dark = '#AF2631',
  default = '#CC3434',
  light = '#EF9784',
  lighter = '#FCE3D6',
}
