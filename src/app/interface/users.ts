import { AuthDTO } from '@app/interface/auth';

export interface UserInterface extends AuthDTO {
  id: string;
  token: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  bookmarks: Array<[]>;
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}
