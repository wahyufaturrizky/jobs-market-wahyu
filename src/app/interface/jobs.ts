import { UserInterface } from "@app/interface/users";

export interface JobDTOInterface {
  name: string;
  desc: string;
  isActive: boolean;
};

export interface JobInterface extends JobDTOInterface {
  id: string;
  upvotes: number;
  downvotes: number;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  user?: UserInterface;
};