import { UserInterface } from "@app/interface/users";

export interface CommentDTOInterface {
  comment: string;
}

export interface CommentInterface extends CommentDTOInterface {
  id: string;
  user: UserInterface;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}