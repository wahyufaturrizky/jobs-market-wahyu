export type AuthType = 'login' | 'register';

export interface AuthDTO {
  username: string;
  password: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  phoneNumber?: string;
  isActive?: boolean;
}
