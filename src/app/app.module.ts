import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import en from '@angular/common/locales/en';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '@app/app-routing.module';
import { AppStoreModule } from '@app/app-store/app-store.module';
import { AppComponent } from '@app/app.component';
import { AuthComponent } from '@app/components/auth/auth.component';
import { ApiService } from '@app/services/api.service';
import { AuthService } from '@app/services/auth.service';
import { UIModule } from '@app/ui/ui.module';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { PageNotFoundComponent } from './screens/page-not-found/page-not-found.component';

registerLocaleData(en);

@NgModule({
  declarations: [AppComponent, AuthComponent, PageNotFoundComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppStoreModule,
    UIModule,
    ReactiveFormsModule,
    StoreRouterConnectingModule.forRoot(),
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }, AuthService, ApiService],
  bootstrap: [AppComponent],
})
export class AppModule {}
