import { Component, OnInit } from '@angular/core';
import { UserInterface } from '@app/interface/users';
import { Store } from '@ngrx/store';
import { AppState } from '../state';
import { LoadUsers } from '../state/user.action';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users: UserInterface[] = [];
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(new LoadUsers());
    this.store
      .select((state) => state)
      .subscribe((val) => {
        this.users = val.users.users;
      });
  }
}
