import { UserInterface } from '@app/interface/users';
import { Action } from '@ngrx/store';

export enum UserActions {
  LOAD_USERS = '[Users] Load users',
  LOAD_USERS_SUCCESS = '[Users] Load users success',
}

export class LoadUsers implements Action {
  readonly type = UserActions.LOAD_USERS;
}

export class LoadUsersSuccess implements Action {
  readonly type = UserActions.LOAD_USERS_SUCCESS;

  constructor(public payload: UserInterface[]) {}
}

export type ActionUsers = LoadUsers | LoadUsersSuccess;
