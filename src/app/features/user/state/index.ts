import { UserInterface } from '@app/interface/users';
import * as Store from '@app/app-store/app-store.module';

export interface UserStateInterface {
  users: UserInterface[];
  loading: boolean;
  loaded: boolean;
}

export interface AppState extends Store.AppState {
  users: UserStateInterface;
}
