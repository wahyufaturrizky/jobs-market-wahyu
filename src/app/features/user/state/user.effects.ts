import { Injectable } from '@angular/core';
import { AddError, RemoveError } from '@app/app-store/actions/errors.action';
import { ApiService } from '@app/services/api.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { AppState } from '.';
import { LoadUsers, LoadUsersSuccess, UserActions } from './user.action';

@Injectable()
export class UserEffects {
  constructor(
    private action$: Actions,
    private store: Store<AppState>,
    private apiService: ApiService
  ) {}

  @Effect()
  loadUsers$: Observable<Action> = this.action$.pipe(
    ofType<LoadUsers>(UserActions.LOAD_USERS),
    tap(() => this.store.dispatch(new RemoveError())),
    mergeMap((action) =>
      this.apiService.getUsers().pipe(
        map((users: any) => new LoadUsersSuccess(users)),
        catchError((err) => of(new AddError(err)))
      )
    )
  );
}
