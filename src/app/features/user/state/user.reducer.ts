import { UserStateInterface } from '.';
import { ActionUsers, UserActions } from './user.action';

const initialState: UserStateInterface = {
  loaded: false,
  loading: false,
  users: [],
};

export const userReducer: (
  state: UserStateInterface,
  action: ActionUsers
) => UserStateInterface = (state = initialState, action) => {
  switch (action.type) {
    case UserActions.LOAD_USERS:
      return { ...state, loaded: false, loading: true };
    case UserActions.LOAD_USERS_SUCCESS:
      return { ...state, users: action.payload, loaded: true, loading: false };
    default:
      return state;
  }
};
