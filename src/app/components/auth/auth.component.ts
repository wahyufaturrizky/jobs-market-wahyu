import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginUser, RegisterUser } from '@app/app-store/actions/auth.action';
import { AppState } from '@app/app-store/app-store.module';
import { AuthDTO } from '@app/interface/auth';
import { Store } from '@ngrx/store';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private notification: NzNotificationService,
    private router: Router
  ) {}
  validateFormLogin!: FormGroup;
  validateFormRegister!: FormGroup;
  isLoading = false;
  isRegister = false;

  submitForm(): void {
    this.isLoading = true;
    const val = this.validateFormLogin.getRawValue() as AuthDTO;
    this.store.dispatch(new LoginUser(val));

    this.store
      .select((state) => state)
      .subscribe((val) => {
        if (val.error.error) {
          this.isLoading = false;
          this.notification.create(
            'error',
            'Failed login',
            val.error.error.error.message
          );
        }

        if (val.auth.user?.token && val.auth.loaded) {
          this.notification.create(
            'success',
            'Success login',
            `welcome ${val.auth.user.username}` || 'Internal server error'
          );
          this.isLoading = false;
          this.router.navigate(['/users']);
        }
      });
  }

  register(): void {
    this.isLoading = true;
    const val = this.validateFormRegister.getRawValue() as AuthDTO;
    this.store.dispatch(new RegisterUser(val));

    this.store
      .select((state) => state)
      .subscribe((val) => {
        if (val.error.error) {
          this.isLoading = false;
          this.notification.create(
            'error',
            'Failed register',
            val.error.error.error.message
          );
        }

        if (val.auth.user?.token && val.auth.loaded) {
          this.isLoading = false;
          this.notification.create(
            'success',
            'Success register',
            `welcome ${val.auth.user.username}`
          );
        }
      });
  }

  ngOnInit(): void {
    const formBuilderGroup = {
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
    };

    const formBuilderRegisterGroup = {
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      email: [null, [Validators.required]],
      phoneNumber: [null, [Validators.required]],
      isActive: [false],
    };

    this.validateFormLogin = this.fb.group(formBuilderGroup);
    this.validateFormRegister = this.fb.group(formBuilderRegisterGroup);
  }
}
