import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommentDTOInterface, CommentInterface } from '@app/interface/comments';
import { JobDTOInterface, JobInterface } from '@app/interface/jobs';
import { UserInterface } from '@app/interface/users';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { AuthService } from '@app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private api: string = environment.api_server + '/api'

  constructor(private http: HttpClient, private auth: AuthService) { }

  private request(method: string, endpoint: string, body?: any): Observable<any> {
    const url = `${this.api}/${endpoint}`
    return this.http.request(method, url, {
      body,
      headers: {
        authorization: `Bearer ${this.auth.token}`
      }
    })
  }

  getUsers(page?: string): Observable<UserInterface> {
    const endpoint = page ? `users?page=${page}` : 'users';
    return this.request('GET', endpoint)
  }

  getUser(userId?: string): Observable<UserInterface> {
    return this.request('GET', `users/${userId}`);
  }

  getJobs(page?: string): Observable<JobInterface> {
    const endpoint = page ? `jobs?page=${page}` : 'jobs';
    return this.request('GET', endpoint)
  }

  getNewestJobs(page?: string): Observable<JobInterface> {
    const endpoint = page ? `jobs/newest?page=${page}` : 'jobs/newest';
    return this.request('GET', endpoint)
  }

  getJob(id: string): Observable<JobInterface> {
    return this.request('GET', `jobs/${id}`)
  }

  createJob(id: string, data: JobDTOInterface): Observable<JobInterface> {
    return this.request('POST', `jobs/${id}`, data)
  }

  updateJob(id: string, data: Partial<JobDTOInterface>): Observable<JobInterface> {
    return this.request('PATCH', `jobs/${id}`, data)
  }

  deleteJob(id: string): Observable<JobInterface> {
    return this.request('DELETE', `jobs/${id}`)
  }

  upvoteJob(id: string): Observable<JobInterface> {
    return this.request('POST', `jobs/${id}/upvote`)
  }

  downvoteJob(id: string): Observable<JobInterface> {
    return this.request('POST', `jobs/${id}/downvote`)
  }

  bookmarkJob(id: string): Observable<UserInterface> {
    return this.request('POST', `jobs/${id}/bookmark`)
  }

  unbookmarkJob(id: string): Observable<CommentInterface> {
    return this.request('DELETE', `jobs/${id}/bookmark`)
  }

  getCommentByJob(jobId: string, page?: string): Observable<CommentInterface> {
    const endpoint = page ? `comment/job/${jobId}?page=${page}` : `comment/job/${jobId}`;
    return this.request('GET', endpoint)
  }

  getCommentByUser(userId: string, page?: string): Observable<CommentInterface> {
    const endpoint = page ? `comment/user/${userId}?page=${page}` : `comment/user/${userId}`;
    return this.request('GET', endpoint)
  }

  getComment(page?: string): Observable<CommentInterface> {
    const endpoint = page ? `comment?page=${page}` : `comment`;
    return this.request('GET', endpoint)
  }

  createComment(JobId: string, data: CommentDTOInterface): Observable<CommentInterface> {
    return this.request('POST', `comment/job/${JobId}`, data)
  }

  deleteComment(id: string): Observable<CommentInterface> {
    return this.request('DELETE', `comment/${id}`)
  }
}
