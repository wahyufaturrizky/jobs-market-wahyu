import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthDTO, AuthType } from '@app/interface/auth';
import { UserInterface } from '@app/interface/users';
import { environment } from '@env/environment';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private api: string = environment.api_server + '/auth';

  constructor(private http: HttpClient) {}

  private auth(authType: AuthType, data: AuthDTO): Observable<UserInterface> {
    return this.http
      .post<UserInterface>(`${this.api}/${authType}`, data, {
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
        },
      })
      .pipe(
        mergeMap((user: UserInterface) => {
          this.token = user.token;
          return of(user);
        })
      );
  }

  login(data: AuthDTO): Observable<UserInterface> {
    return this.auth('login', data);
  }

  register(data: AuthDTO): Observable<UserInterface> {
    return this.auth('register', data);
  }

  whoami(): Observable<UserInterface> {
    return this.http.get<UserInterface>(`${this.api}/whoami`, {
      headers: {
        authorization: `Bearer ${this.token}`,
      },
    });
  }

  get token(): string | null {
    return localStorage.getItem('account_token');
  }

  set token(val: string | null) {
    if (val) {
      localStorage.setItem('account_token', val);
    } else {
      localStorage.clear();
    }
  }
}
